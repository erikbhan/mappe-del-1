package no.ntnu.stud.erikbhan.hospital;

import no.ntnu.stud.erikbhan.hospital.exception.RemoveException;
import no.ntnu.stud.erikbhan.hospital.healthpersonnel.doctor.Surgeon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    private Hospital hospital;

    @BeforeEach
    void setUp() {
        this.hospital = new Hospital("Trondheim sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);
    }

    @Nested
    class RemovePatients {

        @Test
        void removeExistingPatient(){
            try {
                hospital.getDepartments().get(0).remove(new Patient("Inga", "Lykke", ""));
            } catch (RemoveException e){
                e.printStackTrace();
            }
            assertFalse(hospital.getDepartments().get(0).getPatients().contains(new Patient("Inga", "Lykke", "")));
        }

        @Test
        void removeNonExistingPatient(){
            assertThrows(RemoveException.class,() -> hospital.getDepartments().get(0).remove(new Patient("Sjuk", "Sjukesen", "1234567891")));
        }

        @Test
        void removeEmptyPatient(){
            assertThrows(RemoveException.class, () -> hospital.getDepartments().get(0).remove(new Patient("","","")));
        }


    @Nested
    class RemoveEmployees {
            @Test
            void removeExistingEmployee(){
                try {
                    hospital.getDepartments().get(0).remove(new Employee("Rigmor", "Mortis", ""));
                } catch (RemoveException e){
                    e.printStackTrace();
                }
                assertFalse(hospital.getDepartments().get(0).getEmployees().contains(new Employee("Rigmor", "Mortis", "")));
            }

            @Test
            void removeNonExistingEmployee(){
                assertThrows(RemoveException.class, () -> hospital.getDepartments().get(0).remove(new Employee("Doctor", "McDoctorface", "")));
            }

            @Test
            void removeEmptyEmployee(){
                assertThrows(RemoveException.class, () -> hospital.getDepartments().get(0).remove(new Surgeon("","","")));
            }
        }
    }
}