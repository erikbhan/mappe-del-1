package no.ntnu.stud.erikbhan.hospital;

public interface Diagnosable {
    void setDiagnose(String diagnose);
}
