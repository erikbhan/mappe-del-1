package no.ntnu.stud.erikbhan.hospital;

import no.ntnu.stud.erikbhan.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        //Create a hospital object and fill it with test data
        Hospital trondheimSykehus = new Hospital("Trondheim sykehus");
        HospitalTestData.fillRegisterWithTestData(trondheimSykehus);

        System.out.println(trondheimSykehus);

        //Commented code prints out employees for testing purposes
        //System.out.println(trondheimSykehus.getDepartments().get(0).getEmployees());
        trondheimSykehus.getDepartments().get(0).remove(new Employee("Ove", "Ralt", ""));
        //System.out.println(trondheimSykehus.getDepartments().get(0).getEmployees());

        //This code is expected to print the stacktrace, then keep going.
        try {
            trondheimSykehus.getDepartments().get(0).remove(new Patient("Eksisterende", "Ikke", "14"));
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }
}
