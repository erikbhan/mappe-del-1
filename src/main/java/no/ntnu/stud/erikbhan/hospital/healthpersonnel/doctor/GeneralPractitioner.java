package no.ntnu.stud.erikbhan.hospital.healthpersonnel.doctor;

import no.ntnu.stud.erikbhan.hospital.Patient;

/**
 * Subclass of Doctor, may set diagnoses
 */
public class GeneralPractitioner extends Doctor{
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnose(Patient patient, String diagnose) {
        patient.setDiagnose(diagnose);
    }
}
