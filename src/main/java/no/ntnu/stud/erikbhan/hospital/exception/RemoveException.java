package no.ntnu.stud.erikbhan.hospital.exception;

public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    public RemoveException(String str) {
        super(str);
    }
}