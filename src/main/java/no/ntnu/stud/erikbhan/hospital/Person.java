package no.ntnu.stud.erikbhan.hospital;

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * @return full name of the person in this format "lastName, firstName"
     */
    public String getFullName() {
        return lastName + ", " + firstName;
    }


    /**
     * @param o The object to compare to
     * @return a boolean; true if the two compared objects have equal values and are instances of the same class.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        Person person = (Person) o;

        if (!getSocialSecurityNumber().equals(person.getSocialSecurityNumber())) return false;
        return person.getFullName().equals(getFullName());
    }

    /**
     * Returns the persons class, name and social security number as a string; works for all subclasses
     * @return a string.
     */
    @Override
    public String toString() {
        return "\n\t" + getClass().getSimpleName() + ": " + getFullName() + " (SSN: " + socialSecurityNumber + ")";
    }
}
