package no.ntnu.stud.erikbhan.hospital.healthpersonnel.doctor;

import no.ntnu.stud.erikbhan.hospital.Employee;
import no.ntnu.stud.erikbhan.hospital.Patient;

public abstract class Doctor extends Employee {
    Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Only subclasses of the class Doctor may set diagnoses
     * @param patient the patient to set the diagnose for
     * @param diagnosis the diagnose to set for the patient
     */
    public abstract void setDiagnose(Patient patient, String diagnosis);
}
