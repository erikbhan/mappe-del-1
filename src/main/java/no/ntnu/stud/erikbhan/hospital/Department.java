package no.ntnu.stud.erikbhan.hospital;

import no.ntnu.stud.erikbhan.hospital.exception.RemoveException;

import java.util.ArrayList;

public class Department {
    private String departmentName;
    private final ArrayList<Employee> employees;
    private final ArrayList<Patient> patients;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public void addPatient(Patient patient) {
        patients.add(patient);
    }

    /**
     * CODE COMMENTED OUT; NO LONGER IN USE
     */
//    /**
//     * A method to remove a given person from the department without specifying where the person is in the department.
//     *
//     * I saw two approaches to error-handling here; i could use a try-catch-block and handle the error here, or I could
//     * throw the exception. JUnit will not be able see the exception if it is caught/handled here, but I went for the
//     * try-catch-block anyways, because it is what we have learned about recently.
//     *
//     * Because of the try-catch, I have added a boolean return value to test properly.
//     *
//     * @param person the person to attempt to remove from the system.
//     * @return a boolean; true if person is successfully removed.
//     */
//    public boolean remove(Person person) {
//        boolean success = true;
//        try {
//            if (person instanceof Employee) {
//                if (employees.contains(person)) {
//                    employees.remove(person);
//                } else {
//                    throw new RemoveException("Couldn't find " + person + " in emergency department.");
//                }
//            } else if (person instanceof Patient && !patients.isEmpty()) {
//                if (patients.contains(person)) {
//                    patients.remove(person);
//                } else {
//                    throw new RemoveException("Couldn't find " + person + " in " + departmentName + " department.");
//                }
//            } else {
//                throw new RemoveException("Invalid input");
//            }
//        } catch (RemoveException e) {
//            e.printStackTrace();
//            success = false;
//        }
//        return success;
//    }

    /**
     * A method to remove a given person from the department without specifying where the person is in the department.
     *
     * Originally I used a try-catch-block, but I wanted to test for throwing. Additionally, the HospitalClient.java
     * client is required to use a try-catch-block, so for the method to throw an exception it must not handle the
     * exception itself.
     *
     * @param person the person to remove from the department.
     * @throws RemoveException the exception thrown if the person can not be removed for some reason.
     */
    public void remove(Person person) throws RemoveException {
        if (person instanceof Employee && !employees.isEmpty()) {

            if (employees.contains(person)) {
                employees.remove(person);
            } else {
                throw new RemoveException(person + " not found in employees of " + departmentName + " department.");
            }

        } else if (person instanceof Patient && !patients.isEmpty()) {

            if (patients.contains(person)) {
                patients.remove(person);
            } else {
                throw new RemoveException(person + " not found in patients of " + departmentName + " department.");
            }

        } else {
            throw new RemoveException("[INVALID INPUT] " + person + " not found in " + departmentName);
        }
    }

    @Override
    public String toString() {
        return "\n\nDepartment{" +
                "departmentName='" + departmentName + '\'' +
                ", \nemployees=" + employees +
                ", \npatients=" + patients +
                '}';
    }
}
