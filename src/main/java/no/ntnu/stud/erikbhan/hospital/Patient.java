package no.ntnu.stud.erikbhan.hospital;

public class Patient extends Person implements Diagnosable {
    private String diagnose;

    Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
        this.diagnose = "";
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    @Override
    public String toString() {
        return super.toString() + ", diagnosis='" + diagnose + "'";
    }
}
