package no.ntnu.stud.erikbhan.hospital.healthpersonnel.doctor;

import no.ntnu.stud.erikbhan.hospital.Patient;

/**
 * Subclass of Doctor, may set diagnoses
 */
public class Surgeon extends Doctor {
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnose(Patient patient, String diagnose) {
        patient.setDiagnose(diagnose);
    }
}
