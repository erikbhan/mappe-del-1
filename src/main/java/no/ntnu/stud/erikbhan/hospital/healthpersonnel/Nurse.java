package no.ntnu.stud.erikbhan.hospital.healthpersonnel;

import no.ntnu.stud.erikbhan.hospital.Employee;

/**
 * Not a subclass of Doctor, may NOT set diagnoses
 */
public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}
